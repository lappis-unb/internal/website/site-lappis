require 'rails_helper'

# rspec module for testing PagesController
RSpec.describe PagesController, type: :controller do

  # tests for pages#index
  # index page variables testing
  describe "index page tests" do
    it "assings all partners to @partners" do
      get :index

      expect(assigns(:partners)).to match_array(Partner.all)
    end
    
    it "assigns all products to @products" do
      get :index
      
      expect(assigns(:products)).to match_array(Product.all)
    end
  end

  # blog testing
  describe "blog page tests" do
    it "verify if variable is recieving api value" do 
      get :blog

      expect(assigns(:articles)).to_not be_nil
    end
  end
end
