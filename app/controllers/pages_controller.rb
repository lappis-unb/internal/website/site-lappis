# manages the logic behind the application's pages
class PagesController < ApplicationController
  # gets index page variables
  def index
    @index    = true
    @partners = Partner.all.order(:name)
    @products = Product.all.order(:name)
  end

  # gets blog page variables
  # news feed is consumed via api from fga's portal page
  def blog
    lappis_blog_url = 'https://fga.unb.br/api/v1/articles/1590/children'

    @articles = JSON.load(open(lappis_blog_url))['articles'].tap(&:pop)
    @articles.each do |article|
      article['created_at'] = DateTime.parse(article['created_at'])
    end

    @articles.shift
  end

  # picks a news article specifically via api and shows it in pages#article/:id
  def article
    article_url = 'https://fga.unb.br/api/v1/articles/' + params[:id]

    @article = JSON.load(open(article_url))['article']
    @article['created_at'] = DateTime.parse(@article['created_at'])
    @article_url = request.url
  end
end
