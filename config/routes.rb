Rails.application.routes.draw do
  root 'pages#index'
  get '/blog',                to: 'pages#blog'
  get '/blog/:id',            to: 'pages#article'

  # radar and subpages
  get '/radar',               to: 'pages#radar'
end
