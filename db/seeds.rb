Partner.delete_all
Product.delete_all

Product.create(
  [
    {
      name: 'Participa.br',
      description: '',
      photo_path: 'products/participabr.png',
      website: 'https://www.participa.br',
      repository: ''
    },
    {
      name: 'Portal FGA',
      description: 'Portal do campus gama da UnB',
      photo_path: 'products/portal-fga.png',
      website: 'https://fga.unb.com',
      repository: ''
    },
    {
      name: 'Mezuro',
      description: '',
      photo_path: 'products/mezuro.png',
      website: '',
      repository: 'https://github.com/mezuro'
    },
    {
      name: 'Software Público',
      description: '',
      photo_path: 'products/spb.png',
      website: 'https://softwarepublico.gov.br/social',
      repository: ''
    },
    {
      name: 'Noosfero',
      description: '',
      photo_path: 'products/noosfero.png',
      website: '',
      repository: 'https://gitlab.com/noosfero/noosfero'
    },
    {
      name: 'Gestor PSI',
      description: '',
      photo_path: 'products/gestor-psi.png',
      website: 'https://gestorpsi.com.br',
      repository: ''
    },
    {
      name: 'CoLab',
      description: '',
      photo_path: 'products/colab.png',
      website: 'https://colab.interlegis.leg.br',
      repository: ''
    },
    {
      name: 'Portal da Juventude',
      description: '',
      photo_path: 'products/portal-da-juventude.png',
      website: 'https://juventude.gov.br',
      repository: ''
    },
    {
      name: 'Analizo',
      description: '',
      photo_path: 'products/analizo.png',
      website: 'https://analizo.org',
      repository: ''
    }
  ]
)

Partner.create(
  [
    {
      name:     'Paulo Meirelles',
      photo:    'prmm.jpg',
      github:   'https://github.com/prmm',
      gitlab:   'https://gitlab.com/prmm',
      linkedin: 'https://linkedin.com/in/paulormm'
    },
    {
      name:     'Hilmer Rodrigues',
      photo:    'hilmer.jpg',
      github:   '#',
      gitlab:   '#',
      linkedin: '#'
    },
    {
      name:     'Fábio Mendes',
      photo:    'fabio.jpg',
      github:   '#',
      gitlab:   '#',
      linkedin: '#'
    },
    {
      name:     'Carla Rocha',
      photo:    'carla.jpg',
      github:   '#',
      gitlab:   '#',
      linkedin: '#'
    },
    {
      name:     'Edson Alves',
      photo:    'edson.jpg',
      github:   '#',
      gitlab:   '#',
      linkedin: '#'
    },
    {
      name:     'Rodrigo Siqueira',
      photo:    'siqueira.jpg',
      github:   '#',
      gitlab:   '#',
      linkedin: '#'
    },
    {
      name:     'Tallys Martins',
      photo:    'tallys.jpg',
      github:   '#',
      gitlab:   '#',
      linkedin: '#'
    },
    {
      name:     'Arthur Del Esposte',
      photo:    'arthur-del-esposte.jpg',
      github:   'https://github.com/arthurmde',
      gitlab:   'https://gitlab.com/arthurmde',
      linkedin: 'https://www.linkedin.com/in/arthur-del-esposte-97126689/'
    },
    {
      name:     'Dylan Guedes',
      photo:    'dylan.jpg',
      github:   'https://github.com/DylanGuedes',
      gitlab:   'https://gitlab.com/DGuedes',
      linkedin: '#'
    },
    {
      name:     'Lucas Mattioli',
      photo:    'mattioli.jpg',
      github:   'https://github.com/Mattioli',
      gitlab:   'https://gitlab.com/Mattioli',
      linkedin: '#'
    },
    {
      name:     'Charles Oliveira',
      photo:    'charles.jpg',
      github:   'https://github.com/chaws',
      gitlab:   'https://gitlab.com/chaws',
      linkedin: 'https://www.linkedin.com/in/charles-oliveira-55940289/'
    },
    {
      name:     'Simião Carvalho',
      photo:    'simiao.jpg',
      github:   'https://github.com/simiaosimis',
      gitlab:   'https://gitlab.com/simiaosimis',
      linkedin: 'https://www.linkedin.com/in/simião-carvalho-b8856b113'
    },
    {
      name:     'Pedro de Lyra',
      photo:    'pedro.jpg',
      github:   'https://github.com/pedrodelyra',
      gitlab:   'https://gitlab.com/pedrodelyra',
      linkedin: 'https://www.linkedin.com/in/pedro-de-lyra-pereira-10850113b/'
    },
    {
      name:     'Victor Matias',
      photo:    'victor-matias.jpg',
      github:   'https://github.com/matias310396',
      gitlab:   'https://gitlab.com/matias310396/',
      linkedin: '#'
    },
    {
      name:     'Vitor Barbosa',
      photo:    'vitor-barbosa.jpg',
      github:   'https://github.com/vitorbaraujo',
      gitlab:   'https://gitlab.com/vitorbarbosa',
      linkedin: '#'
    },
    {
      name:     'Renata Soares',
      photo:    'renata.jpg',
      github:   'https://github.com/renatasoares',
      gitlab:   'https://gitlab.com/renatasoares',
      linkedin: '#'
    },
    {
      name:     'André de Sousa',
      photo:    'AndreSousa.jpg',
      github:   'https://github.com/andre-filho',
      gitlab:   'https://gitlab.com/andre.filho',
      linkedin: '#'
    },
    {
      name:     'Arthur Assis',
      photo:    'ArthurAssis.jpg',
      github:    'https://github.com/arthur0496',
      gitlab: '  https://gitlab.com/arthur0496',
      linkedin: '#'
    },
    {
      name:     'Arthur Diniz',
      photo:    'ArthurDiniz.jpg',
      github:   'https://github.com/',
      gitlab:   'https://gitlab.com/',
      linkedin: '#'
    },
    {
      name:     'Bruna Pinos',
      photo:    'BrunaPinos.jpg',
      github:   'https://github.com/brunapinos',
      gitlab:   'https://gitlab.com/brunapinos',
      linkedin: '#'
    },
    {
      name:     'Clarissa Borges',
      photo:    'ClarissaBorges.jpg',
      github:   'https://github.com/clarissalimab',
      gitlab:   'https://gitlab.com/clarissalimab',
      linkedin: '#'
    },
    {
      name:     'Guilherme Lacerda',
      photo:    'GuilhermeLacerda.jpg',
      github:   'https://github.com/guilacerda',
      gitlab:   'https://gitlab.com/guila',
      linkedin: '#'
    },
    {
      name:     'João Robson',
      photo:    'JoaoRobson.jpg',
      github:   'https://github.com/joaorobson',
      gitlab:   'https://gitlab.com/joaorobson',
      linkedin: '#'
    },
    {
      name:     'Lorrany Azevedo',
      photo:    'LorranyAzevedo.jpg',
      github:   'https://github.com/lorryaze',
      gitlab:   'https://gitlab.com/lorryaze',
      linkedin: '#'
    },
    {
      name:     'Lucas Malta',
      photo:    'LucasMalta.jpg',
      github:   'https://github.com/lucasssm',
      gitlab:   'https://gitlab.com/lucasssm',
      linkedin: '#'
    },
    {
      name:     'Lucas Martins',
      photo:    'LucasMartins.jpg',
      github:   'https://github.com/martinslucas',
      gitlab:   'https://gitlab.com/lucasmartins',
      linkedin: '#'
    },
    {
      name:     'Matheus Richard',
      photo:    'MatheusRichard.jpg',
      github:   'https://github.com/MatheusRich',
      gitlab:   'https://gitlab.com/MatheusRichard',
      linkedin: '#'
    },
    {
      name:     'Thalisson Barreto',
      photo:    'ThalissonBarreto.jpg',
      github:   'https://github.com/ThalissonMelo',
      gitlab:   'https://gitlab.com/ThalissonMelo',
      linkedin: '#'
    },
    {
      name:     'Victor Moura',
      photo:    'VictorMoura.jpg',
      github:   'https://github.com/victorcmoura',
      gitlab:   'https://gitlab.com/victor_cmoura',
      linkedin: 'victorcmoura'
    },
    {
      name:     'Vitor Falcão',
      photo:    'VitorFalcao.jpg',
      github:   'https://github.com/vitorfhc',
      gitlab:   'https://gitlab.com/vitorfhc',
      linkedin: '#'
    },
    {
      name:     'Gabriel Silva',
      photo:    'GabrielSilva.jpg',
      github:   'https://github.com/gabrielssilva',
      gitlab:   'https://gitlab.com/gabrielssilva',
      linkedin: '#'
    },
    {
      name:     'Victor Henrique',
      photo:    'VictorHenrique.jpg',
      github:   'https://github.com/victorhmf',
      gitlab:   'https://gitlab.com/victor.hmf',
      linkedin: '#'
    },
    {
      name:     'Luan Guimarães',
      photo:    'LuanGuimaraes.jpg',
      github:   'https://github.com/luanguimaraesla',
      gitlab:   'https://gitlab.com/',
      linkedin: '#'
    },
    {
      name:     'Álax Alves',
      photo:    'AlaxAlves.jpg',
      github:   'https://github.com/alaxalves',
      gitlab:   'https://gitlab.com/alaxalves',
      linkedin: '#'
    },
    {
      name:     'Matheus Miranda',
      photo:    'MatheusMiranda.jpg',
      github:   'https://github.com/matheusmiranda',
      gitlab:   'https://gitlab.com/matheusmiranda',
      linkedin: '#'
    },
    {
      name:     'Fábio Texeira',
      photo:    'FabioTex.jpg',
      github:   'https://github.com/fabio1079',
      gitlab:   'https://gitlab.com/fabio1079',
      linkedin: '#'
    },
    {
      name:     'Daniel Marinho',
      photo:    'DanielMarinho.png  ',
      github:   'https://github.com/danielhmarinho',
      gitlab:   'https://gitlab.com/danielhmarinho',
      linkedin: '#'
    }
  ]
)
