# generated migration files
class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.string :photo_path
      t.string :website
      t.string :repository

      t.timestamps
    end
  end
end
